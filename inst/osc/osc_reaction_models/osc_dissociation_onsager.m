## Copyright (C) 2009 Carlo de Falco <carlo DOT defalco AT gmail DOT com>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; If not, see <http://www.gnu.org/licenses/>.


## -*- texinfo -*-
## @deftypefn {Function File} {@var{coeff} =} osc_dissociation_onsager (@var{Phib}, @var{a})  
## Onsager-type exciton dissociation model. @var{Phib} is the barrier
## potential and @var{a} is the initial separaion.
##
## @end deftypefn

## Author: Carlo de Falco <carlo DOT defalco AT gmail DOT com>

function coeff = osc_dissociation_onsager (Phib, a)

  coeff = @(data, E) __onsager__ (data.gamma(data, E), Phib, a, data.temp, E, data.er);

endfunction

function ret = __onsager__ (gamma, Phib, a, temp, E, er)

  persistent e0 = physical_constant("electric constant");
  persistent q  = physical_constant("elementary charge");
  persistent Kb = physical_constant("boltzmann constant");

  c21 = 3* gamma * exp(-Phib/(Kb*temp/q)) / (4*pi*a^3); 
  c22 = q/(8*pi*er*e0*(Kb*temp/q)^2); 
  b   = c22 .* abs(E);
  ret = c21 .* (1 + b  + b.^2/3 + b.^3/18 + b.^4/180);

endfunction
