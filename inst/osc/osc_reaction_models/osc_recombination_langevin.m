## Copyright (C) 2009 Carlo de Falco <carlo DOT defalco AT gmail DOT com>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; If not, see <http://www.gnu.org/licenses/>.


## -*- texinfo -*-
## @deftypefn {Function File} {@var{coeff} =} osc_recombination_langevin ()  
## Langevin-type recombination model, this models does not have any
## user-set parameters.
##
## @end deftypefn

## Author: Carlo de Falco <carlo DOT defalco AT gmail DOT com>

function coeff = osc_recombination_langevin ()

  persistent e0 = physical_constant("electric constant");
  persistent q  = physical_constant("elementary charge");

  coeff = @(data, E) q*(data.un(data, E)+data.up(data, E))/(e0*data.er);

endfunction
