## Copyright (C) 2009 Carlo de Falco <carlo DOT defalco AT gmail DOT com>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; If not, see <http://www.gnu.org/licenses/>.


## -*- texinfo -*-
## @deftypefn {Function File} {} osc_demo_daspk_low_intensity ()  
## demo for the OSC simulator. Model and parameters are those of Fig. 4
## in the paper: "I.Hwang and N.Greenham. Modeling photocurrent
## transients in organic solar cells. Nanotechnology, 19:424012 (8pp),
## 2008". 
## @end deftypefn

## Author: Carlo de Falco <carlo DOT defalco AT gmail DOT com>

function [data, output, jtot] = osc_demo_daspk_low_intensity ()

  ##------------
  ## Define Input data
  ##------------
  
  ##---
  ##1# set device geometry and spatial mesh
  ##---
  data = osc_fem_set ([], "length", 70e-9);
  data = osc_fem_set (data, "mesh", osc_fem_uniform_mesh (31));

  ##---
  ##2# chose DAE solver and set solver parameters 
  ##---
  data = osc_tst_set (data, "solver", "daspk");
  data = osc_tst_set (data, "time slot", [0, 13e-6]);
  data = osc_tst_set (data, "number of time steps", 51);
  data = osc_nls_set (data, "residual scaling coefficients",[1e10, 1e10, 1e8, 1e-16],
		      "state scaling coefficients",  [1e19, 1e19, 1e20, 1]);
  
  ##---
  ##3# set device material properties 
  ##---
  data = osc_material_properties (data, "relative permittivity", 4);
  
  data = osc_material_properties (data, "electron mobility model", 
				  osc_mobility_constant (2e-8));
  data = osc_material_properties (data, "hole mobility model", 
				  osc_mobility_constant (2e-8));
  data = osc_material_properties (data, "recombination rate model", 
				  osc_recombination_langevin());
  data = osc_material_properties (data, "pair dissociation model", 
				  osc_dissociation_onsager (0.27, 1.3e-9));
  data = osc_material_properties (data, "generation efficiency", 1e5);
  data = osc_material_properties (data, "contact injection model", 
				  osc_injection_scottmalliaras (0.5, 1e27));
  data = osc_material_properties (data, "light induced pair generation", 
				  sole_time_signal ("step", [1e-6, 0, 4.3e26]));
  
  data = osc_simulation_conditions (data, "temperature", 293);
  data = osc_simulation_conditions (data, "applied voltage", -.5);
  
  ##---
  ## Run the simulation
  ##---
  
  ##---
  ##1# initialize simulator 
  ##---
  data  = osc_transient_init (data);
  
  ##---
  ##2# transient simulation
  ##---
  output = osc_transient_run  (data);
  
  ##---
  ##3# post processing
  ##---
  [jtot, t] = osc_output_get (data, output, "contact current");
  figure(1), plot(t, jtot(1,:), t, -jtot(2,:));
  xlabel("t [s]"); ylabel("J [A/m**2]");
  
  [Xm, t]   = osc_output_get (data, output, "average pair density");
  figure(2), plot(t, Xm);
  xlabel("t [s]"); ylabel("<X> [1/m**3]");
  
  [E, t, x]    = osc_output_get (data, output, "electric field");
  figure(3), plot(x, E(end,:));
  xlabel("x [m]"); ylabel("E [V/m]");
  
  [n, t, x]    = osc_output_get (data, output, "electron density");
  figure(3); plot(x, n); 
  xlabel("x [m]"); ylabel("n [1/m**3]");
  
endfunction