## Copyright (C) 2009 Carlo de Falco <carlo DOT defalco AT gmail DOT com>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; If not, see <http://www.gnu.org/licenses/>.


## -*- texinfo -*-
## @deftypefn {Function File} {@var{input_data} =} osc_material_properties (@var{input_data}, [ @var{property}, @var{value}, ...] )  
## Set device material properties.
##
## Available properties:
## @itemize @minus
## @item @code{"electron mobility model"}
## @item @code{"hole mobility model"}
## @item @code{"recombination rate model"}
## @item @code{"pair dissociation model"}
## @item @code{"generation efficiency"}
## @item @code{"contact injection model"}
## @item @code{"light induced pair generation"}
## @item @code{"relative permittivity"}
## @end itemize
## @end deftypefn

## Author: Carlo de Falco <carlo DOT defalco AT gmail DOT com>


function data = osc_material_properties (data, varargin)

  narg = length(varargin);
  if (mod (narg,2) != 0 || narg < 2)
    print_usage ();
    return;
  endif


  for iarg = 1:2:narg-1
    property = varargin{iarg};
    value    = varargin{iarg+1};
    switch property
      case "electron mobility model"
	data.un = value;
      case "hole mobility model"
	data.up = value;
      case "recombination rate model"
	data.gamma = value;
      case "pair dissociation model"
	data.kdiss = value;
      case "generation efficiency"
	data.krec = value;
      case "contact injection model"
	data.bccoeffs = value;
      case "light induced pair generation"
	data.G = value;
      case "relative permittivity"
	data.er = value;
      otherwise
	error(sprintf("unknown property: %s", property))
    endswitch
  endfor


endfunction