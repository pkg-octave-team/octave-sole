## Copyright (C) 2009 Carlo de Falco <carlo DOT defalco AT gmail DOT com>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*-
## @deftypefn {Function File} {} osc_demo_daspk_exp_absorption ()  
## demo for the OSC simulator.
##
## The device is a 1um long vertical BHJ, the light absorption
## coefficient is 1/1e-7 m.
##
## The other model and parameters are those of Fig.
## 6(a) in the paper: "I.Hwang and N.Greenham. Modeling photocurrent
## transients in organic solar cells. Nanotechnology, 19:424012 (8pp),
## 2008". 
## 
## @end deftypefn

## Author: Carlo de Falco <carlo DOT defalco AT gmail DOT com>

function [data, output] = osc_demo_fsolve_exp_absorption ()
  
  ##------------
  ## Define Input data
  ##------------
  
  ##---
  ##1# set device geometry and spatial mesh
  ##---
  data = osc_fem_set ([], "length", 1.5e-6);
  data = osc_fem_set (data, "mesh", osc_fem_uniform_mesh (61));
  
  ##---
  ##2# chose DAE solver and set solver parameters 
  ##---
  data = osc_tst_set (data, "solver", "daspk");
  data = osc_nls_set (data, "solver", "fsolve");
  data = osc_tst_set (data, "time slot", [0, 2e-1]);
  data = osc_tst_set (data, "number of time steps", 1);
  data = osc_nls_set (data, "residual scaling coefficients",[1e31, 1e31, 1, 1e8],
		      "state scaling coefficients",  [1e19, 1e19, 1e20, 5e-3]);
  
  ##---
  ##3# set device material properties 
  ##---
  data = osc_material_properties (data, "relative permittivity", 4);
  
  data = osc_material_properties (data, "electron mobility model", 
				  osc_mobility_constant (6.25e-10));
  data = osc_material_properties (data, "hole mobility model", 
				  osc_mobility_constant (1.27e-10));
  data = osc_material_properties (data, "recombination rate model", 
				  osc_recombination_langevin());
  data = osc_material_properties (data, "pair dissociation model", 
				  osc_dissociation_onsager (0.27, 1.3e-9));
  data = osc_material_properties (data, "generation efficiency", 1e7);
  data = osc_material_properties (data, "contact injection model", 
				  osc_bc_maxwellboltzmann (.5, 1e27));
  data = osc_material_properties (data, "light induced pair generation", 
				  sole_time_space_signal ("step",  [0, 0, 3.89e27],
				  "exponential absorption", 3.5e6));
  
  data = osc_simulation_conditions (data, "temperature", 293);
  data = osc_simulation_conditions (data, "applied voltage", .005);
  
  ##---
  ## Run the simulation
  ##---
  
  ##---
  ##1# initialize simulator 
  ##---
  data  = osc_steady_state_init (data);
  
  ##---
  ##2# nonlinear simulation
  ##---
  output = osc_steady_state_run (data);
  
  printf("residual = %g\n", norm(output.yval,inf))
  
  ##---
  ##3# post processing
  ##---
  data.ydot = 0*output.y;
%%  [jtot, t]   = osc_output_get (data, output, "contact current");
  [Xm, t]     = osc_output_get (data, output, "average pair density");
  [E, t, xm]  = osc_output_get (data, output, "electric field");
  [V, t, x]   = osc_output_get (data, output, "electric potential");
  [n, t, x]   = osc_output_get (data, output, "electron density");
 

endfunction
