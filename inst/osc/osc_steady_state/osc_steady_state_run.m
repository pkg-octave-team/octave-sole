## Copyright (C) 2009 Carlo de Falco <carlo DOT defalco AT gmail DOT com>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*-
## @deftypefn {Function File} {@var{output} =} osc_steady_state_run (@var{data})  
## Run a steady state simulation.
##
## @end deftypefn

## Author: Carlo de Falco <carlo DOT defalco AT gmail DOT com>

function output = osc_steady_state_run (data)

  switch data.nls
    case "fsolve"
      [output.y, output.yval, output.info] = ...
      fsolve (data.fun, data.x0, optimset ('TolX', 1e-6, 'TolFun', 1e-3, 'Jacobian', 'on', 'AutoScaling', 'on'));
      output.y = output.y.';
      output.yval = output.yval.';
      if (!(output.info == 1))
	warning("fsolve returned the error code INFO=%d, type ""help fsolve"" for more details", output.info)
      endif
    otherwise
      error("unsupported solver")
  endswitch

endfunction
