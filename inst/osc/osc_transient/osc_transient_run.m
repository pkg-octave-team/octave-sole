## Copyright (C) 2009 Carlo de Falco <carlo DOT defalco AT gmail DOT com>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*-
## @deftypefn {Function File} {@var{output} =} osc_transient_run (@var{data})  
## Run a transient simulation.
##
## @end deftypefn

## Author: Carlo de Falco <carlo DOT defalco AT gmail DOT com>

function output = osc_transient_run (data)

  switch data.tst
    case "daspk"
      [output.y, output.ydot] = daspk (data.fun, data.x0, data.xdot0, data.t);
    case "ode15i"
      [output.t, output.y] = ode15i (data.fun, data.t, data.x0, data.xdot0, data.odeopt);      

      tt = linspace (output.t(1), output.t(end), numel(output.t)*5);
      yy = interp1 (output.t, output.y, tt);
      pp = ppder (interp1 (tt, yy, "pchip", "pp"));
      output.ydot = ppval (pp, output.t).';
      
    otherwise
      error("unsupported solver")
  endswitch

endfunction
