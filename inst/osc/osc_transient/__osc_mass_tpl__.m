## Copyright (C) 2009 Carlo de Falco 
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*-
## @deftypefn {Function File} {@var{mass} = }  __osc_mass_tpl__ @ (@var{N},@
## @var{m}, @var{sc}, @var{scl}) 
##
## Undocumented internal function.
## @end deftypefn

## Author: Carlo de Falco <carlo DOT defalco AT gmail DOT com>

function retval =  __osc_mass_tpl__ ( data )
 
  N  = data.Nx;
  sc = data.sc; 
  scl= data.scl;
  h  = diff(data.x);
  M  = ([0;h]+[h;0])/2;

  sz     = 4*N-2;

  retv = [(sc(1)/scl(1))*M(:); (sc(2)/scl(2))*M(:); (sc(3)/scl(3))*M(:)];
  retv(sz) = 0;

  retval = spdiag (retv);

endfunction

function M = spdiag (v)
  M = spdiags (v, 0, numel(v), numel(v));
endfunction
