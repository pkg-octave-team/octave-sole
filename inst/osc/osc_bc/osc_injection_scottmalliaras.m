## Copyright (C) 2009 Carlo de Falco <carlo DOT defalco AT gmail DOT com>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*-
## @deftypefn {Function File} {@var{coeffs} =} osc_injection_scottmalliaras (@var{Phib}, @var{N0}, [@var{Phib_h}, @var{P0} ])  
## Define the model for charge injection and surface recombination at
## the contacts as described in the papers [1,2]. @var{Phib} is the
## injection barrier potential and @var{N0} is the site density.
## The optional parameters @var{Phib_h} and @var{P0} are the barrier
## potential and site density for holes, if values are not provided the
## same values as for electrons are assumed.
##
## [1]	J.Barker, C.Ramsdale, and N.Greenham. Modeling the
## current-voltage characteristics of bilayer polymer photovoltaic
## devices. Physical Review B, 67:075205 (9pp), 2003. 
## [2]	J.Campbell Scott and G.Malliaras. Charge injection and
## recombination at the methal-organic interface. Chemical Physics
## Letters, 299:115-119, 1999. 
##
## @end deftypefn

## Author: Carlo de Falco <carlo DOT defalco AT gmail DOT com>


function coeffs = osc_injection_scottmalliaras (Phib, N0, Phib_h, P0)

  if ((nargin!=2) && (nargin!=4))
    print_usage();
  endif

  if (nargin==2)
    Phib_h = Phib;
    P0 = N0;
  endif


  coeffs = @(data, E) [__alfa__(data.un(data, abs(E)), data.er, -E, data.temp)(:)';
		       __beta__(E)(:)';
		       __gamma_fun__(data.un(data, abs(E)), data.er, -E, data.temp, N0, Phib)(:)';
		       __alfa__(data.up(data, abs(E)), data.er, E, data.temp)(:)';
		       __beta__(-E)(:)';
		       __gamma_fun__(data.up(data, abs(E)), data.er, E, data.temp, P0, Phib_h)(:)'];

endfunction

function ret = __alfa__ (mu, er, E, temp)

  persistent e0 = physical_constant("electric constant");
  persistent Kb = physical_constant("boltzmann constant");
  persistent q  = physical_constant("elementary charge");

  rc  = q / (4*pi*er*e0*Kb*temp/q);
  f   = rc * E(E>0) / (Kb*temp/q);
  psi = 1./f + 1./sqrt(f) - sqrt(1+2*sqrt(f))./f;
  ret = zeros(size(E)); 
  ret(E>0)  = mu(E> 0).*(4*pi*er*e0*(Kb*temp/q)^2) .* (psi.^(-2) - f);
  ret(E<=0) = mu(E<=0).*(4*pi*er*e0*(Kb*temp/q)^2) .* 4;
  ret /= q;

endfunction

function ret = __beta__ (E)
  ret =ones(size(E));
endfunction

function ret = __gamma_fun__ (mu, er, E, temp, N0, Phib)

  persistent e0 = physical_constant("electric constant");
  persistent Kb = physical_constant("boltzmann constant");
  persistent q  = physical_constant("elementary charge");
  
  rc  = q / (4*pi*er*e0*(Kb*temp/q));
  f   = rc * E(E>0) / (Kb*temp/q);
  
  shift = E(E<=0)*rc/4;
  ret = zeros(size(E)); 
  
  ret(E>0) = mu(E>0).*(16*pi*er*e0*N0*(Kb*temp/q)^2) .* ...
      exp(-(Phib )/(Kb*temp/q)) .* exp (sqrt(f));
  
  ret(E<=0) =  mu(E<=0).*(16*pi*er*e0*N0*(Kb*temp/q)^2) .* ...
      exp(-(Phib - shift)/(Kb*temp/q));
  
endfunction

