## Copyright (C) 2009 Carlo de Falco <carlo DOT defalco AT gmail DOT com>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*-
## @deftypefn {Function File} {@var{coeffs} =} osc_injection_scottmalliaras (@var{Phib}, @var{N0}, [@var{Phib_h}, @var{P0} ])  
## Define the model for charge injection and surface recombination at
## the contacts as described in the papers [1,2]. @var{Phib} is the
## injection barrier potential and @var{N0} is the site density.
## The optional parameters @var{Phib_h} and @var{P0} are the barrier
## potential and site density for holes, if values are not provided the
## same values as for electrons are assumed.
##
## [1]	J.Barker, C.Ramsdale, and N.Greenham. Modeling the
## current-voltage characteristics of bilayer polymer photovoltaic
## devices. Physical Review B, 67:075205 (9pp), 2003. 
## [2]	J.Campbell Scott and G.Malliaras. Charge injection and
## recombination at the methal-organic interface. Chemical Physics
## Letters, 299:115-119, 1999. 
##
## @end deftypefn

## Author: Carlo de Falco <carlo DOT defalco AT gmail DOT com>


function coeffs = osc_bc_maxwellboltzmann (Phib, N0, Phib_h, P0)

  persistent Kb = physical_constant("boltzmann constant");
  persistent q  = physical_constant("elementary charge");

  if ((nargin!=2) && (nargin!=4))
    print_usage();
  endif

  if (nargin==2)
    Phib_h = Phib;
    P0 = N0;
  endif


  coeffs = @(data, E) [ones(size(E))(:)';
		       zeros(size(E))(:)';
		       N0*exp(-q*Phib/(Kb*data.temp))*ones(size(E))(:)';
		       ones(size(E))(:)';
		       zeros(size(E))(:)';
		       P0*exp(-q*Phib_h/(Kb*data.temp))*ones(size(E))(:)'];

endfunction


