## Copyright (C) 2009 Carlo de Falco <carlo DOT defalco AT gmail DOT com>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; If not, see <http://www.gnu.org/licenses/>.


## -*- texinfo -*-
## @deftypefn {Function File} {@var{mu} =} osc_mobility_poolefrenkel (@var{u0}, @var{de0}, @var{Teff}, @var{beta}, @var{gamma}, @var{vmax})  
## Poole-Frenkel mobility model:
## @iftex
## @tex
## $$\mu_{pf} = \mu_0 \exp (- \frac{de_0}{K_b T_eff} + \sqrt{|E|}
## (\frac{\beta}{K_b T_eff}-\gamma)$$
## $$ \mu = \frac{1}{\frac{1}{\mu_{pf}}+\frac{1}{\mu_{lim}}} $$
## $$ \mu_{lim} = \frac{v_max}{|E|}$$ 
## @end tex
## @end iftex
## @ifnottex
##
## @code{ @var{mupf} = @var{u0} * exp (- (@var{de0}/(K_b *@var{Teff})) +@
## sqrt(abs(E)) * ((@var{beta}/(Kb @var{T_eff}))- @var{gamma}) }
##
## @code{ @var{mulim} = @var{vmax}/abs(E) }
##
## @code{ @var{mu} = 1/((1/@var{mupf})+(1/@var{mulim})) }
## @end ifnottex
## @end deftypefn

## Author: Carlo de Falco <carlo DOT defalco AT gmail DOT com>

function mu = osc_mobility_poolefrenkel (u0, de0, Teff, beta, gamma, vmax)  

  mu = @(data, E) __poolefrenkel__ (E, u0, de0, Teff, beta, gamma, vmax);

endfunction

function mu = __poolefrenkel__ (E, u0, de0, Teff, beta, gamma, vmax)   

  persistent Kb = physical_constant("boltzmann constant");

  mulim = vmax./abs(E);
  mupf  = u0 .* exp ( - (de0./(Kb.*Teff)) + abs(E) .*  ...
		     ((beta./(Kb.*Teff) - gamma)));
  mu    = 1./(1./mulim + 1./mupf);

endfunction
