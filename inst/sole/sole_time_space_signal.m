## Copyright (C) 2009 Carlo de Falco <carlo DOT defalco AT gmail DOT com>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*-
## @deftypefn {Function File} {@var{f} =} sole_time_space_signal (@var{t_type}, @var{t_parameters}, @var{t_type}, @var{t_parameters})  
## Define a function of time and space.
##
## Available function classes are:
## @itemize @minus
## @item @code{"step"}
## @end itemize
## @end deftypefn

## Author: Carlo de Falco <carlo DOT defalco AT gmail DOT com>

function fg = sole_time_space_signal( t_type, t_parameters, s_type, s_parameters)

  if (nargin != 4)
    print_usage ();
    return;
  endif

  switch t_type
    case "step"
      f = @(t)  __stepfunction__ ...
	  (t, t_parameters(1), t_parameters(2), t_parameters(3));
    otherwise
      error(sprintf("unknown function class: %s", t_type))
  endswitch

  switch s_type
    case "exponential absorption"
      g = @(x)  __exp_abs__  (x, s_parameters(1));
    otherwise
      error(sprintf("unknown function class: %s", s_type))
  endswitch

  fg = @(t,x) f(t) .* g(x);

endfunction

function G = __exp_abs__ (x, coeff)
  G_in  = exp(coeff*(x(1)-x));
  G_ref = G_in(end) .* exp(coeff*(x-x(end)));
  G = G_ref + G_in;
endfunction