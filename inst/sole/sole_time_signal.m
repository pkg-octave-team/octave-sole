## Copyright (C) 2009 Carlo de Falco <carlo DOT defalco AT gmail DOT com>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*-
## @deftypefn {Function File} {@var{f} =} sole_time_signal (@var{type}, @var{parameters} )  
## Define a function of time.
##
## Available function classes are:
## @itemize @minus
## @item @code{"step"}
## @end itemize
## @end deftypefn

## Author: Carlo de Falco <carlo DOT defalco AT gmail DOT com>

function f = sole_time_signal( ftype, parameters)

  if (nargin != 2)
    print_usage ();
    return;
  endif

  switch ftype
    case "step"
      f = @(t, x)  __stepfunction__ (t, parameters(1), parameters(2), parameters(3)) * ones(size (x));
    otherwise
      error(sprintf("unknown function class: %s", ftype))
  endswitch

endfunction

